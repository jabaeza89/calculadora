
function sumar() {
    const n1 = Number(document.querySelector('.numero1').value);
    const n2 = Number(document.querySelector('.numero2').value);
    alert(n1 + n2);
    console.log(n1 + n2);
}

document.querySelector('.sumar').addEventListener('click', sumar)

function restar() {
    const n1 = Number(document.querySelector('.numero1').value);
    const n2 = Number(document.querySelector('.numero2').value);
    alert(n1 - n2);
    console.log(n1 - n2);
}

document.querySelector('.restar').addEventListener('click', restar)

function multiplicar() {
    const n1 = Number(document.querySelector('.numero1').value);
    const n2 = Number(document.querySelector('.numero2').value);
    alert(n1 * n2);
    console.log(n1 * n2);
}

document.querySelector('.multiplicar').addEventListener('click', multiplicar)

function dividir() {
    const n1 = Number(document.querySelector('.numero1').value);
    const n2 = Number(document.querySelector('.numero2').value);
    alert(n1 / n2);
    console.log(n1 / n2);
}

document.querySelector('.dividir').addEventListener('click', dividir)

function sumarCuadrados() {
    const n1 = Number(document.querySelector('.numero1').value);
    const n2 = Number(document.querySelector('.numero2').value);
    alert((n1*n1)+(n2*n2));
    console.log((n1*n1)+(n2*n2));
}

document.querySelector('.sumarCuadrados').addEventListener('click', sumarCuadrados)